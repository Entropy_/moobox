package libs

type celPosS struct {
	Y []string
	X []string
}
type AttributedToS struct {
	TypeS string
	Name  string
}
type AttachmentS struct {
	TypeS   string
	Content string
	Url     string
}
type AudienceS struct {
	TypeS string
	Name  string
}
type ContentS struct {
	Content   string
	Summary   string
	TypeS     string
	MediaType string
}
type ContextS struct {
	Context string
	Summary string
	TypeS   string
	Items   []string
}
type NameS struct {
	Context string
	TypeS   string
	Name    string
}
type EndTimeS struct {
	Context   string
	TypeS     string
	Name      string
	StartTime string
	EndTime   string
}
type GeneratorS struct {
	TypeS string
	Name  string
}
type IconS struct {
	TypeS  string
	Name   string
	Url    string
	Width  int
	Height int
}
type InReplyToS struct {
	Summary string
	TypeS   string
	Content string
}
type LocationS struct {
	Name  string
	TypeS string
	Place string
}
type RepliesS struct {
	TypeS      string
	TotalItems int
	Items      []ObjectS
}
type TagS struct {
	TypeS string
	Id    string
	Name  string
}
type ObjectS struct {
	Attachment   AttachmentS
	AttributedTo AttributedToS
	Audience     AudienceS
	Content      ContentS
	Context      ContextS
	Name         NameS
	EndTime      EndTimeS
	Generator    GeneratorS
	Icon         IconS
	InReplyTo    InReplyToS
	Location     LocationS
	//There is no use for this right now
	Preview string
	//
	Published string
	Replies   RepliesS
	StartTime string
	Summary   string
	Tag       TagS
	Updated   string
	Url       string
	To        string
	Bto       string
	Cc        string
	Bcc       string
	MediaType string
	Duration  string
	Position  celPosS
}
