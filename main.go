package main

import (
	"bufio"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	flour "gitlab.com/entro-pi/loaf"
	"gitlab.com/entro-pi/ogopogo/libs"
)

var hostname string

func validateLogin(signature string, file []byte, actors []libs.ActorS) bool {

	if signature != "" {
		bufferedReader, err := os.Create("tests/florp")

		if err != nil {
			fmt.Println("Error creating outfile")
		}
		bufferedReader.Write(file)
		bufferedReader.Close()
		decrypter := exec.Command("/bin/sh", "-c", "echo crashandburn | gpg --passphrase-fd 0 -o florpfile florp")
		decrypter.Dir = "tests/"
		decrypter.Run()
		defer os.Remove("tests/florp")
		florpFile, err := os.Open("tests/florpfile")
		defer os.Remove("tests/florpfile")
		florpReader := io.Reader(florpFile)
		florpScanner := bufio.NewScanner(florpReader)

		for florpScanner.Scan() {
			//fmt.Println(florpScanner.Text())
			if florpScanner.Text() == signature {
				florptext := strings.Split(florpScanner.Text(), ":")
				for i := range actors {
					if florptext[0] == actors[i].PreferredUsername {
						if florptext[1] == actors[i].Password {

							//Uncomment the line to clone the user at login
							//createUser(florptext[0], florptext[1])
							fmt.Println("LOGGED THE HECK IN")
							return true
						}
					}
				}
			}
		}

		if err != nil {
			fmt.Println("\033[2;255;0;0mERROR\033[0m")
			fmt.Println(err)
			return false
		}

	}

	return false
}
func createMobile(username string) bool {
	//AVATARCOMMAND
	hostname = "http://snowcrash.network"
	UID := UIDMaker("user")
	userFile, err := os.Create("tests/mobiles/" + UID)
	if err != nil {
		fmt.Println("Error creating userfile")
	}
	var userActor libs.MobileS
	userActor.PreferredUsername = username
	userActor.Str = 15
	userActor.Dex = 15
	userActor.Int = 15
	userActor.Wis = 15
	userActor.Con = 15
	userActor.Tec = 1
	userActor.Inventory = map[int]string{
		0: "A mobile sticker",
	}
	userActor.Equipment = map[string]string{
		"body": "Fur",
	}
	userActor.Inbox = hostname + "#inbox/" + username
	userActor.Outbox = hostname + "#outbox/" + username
	userActor.Following = hostname + "#following/" + username
	userActor.Followers = hostname + "#followers/" + username
	userData, err := json.Marshal(&userActor)
	if err != nil {
		fmt.Println("Error marshalling json.")
		return false
	}
	userFile.Write(userData)
	userFile.Sync()
	return true

}
func createUser(username string, password string) bool {
	hostname = "http://snowcrash.network"
	UID := UIDMaker("user")
	userFile, err := os.Create("tests/" + UID)
	if err != nil {
		fmt.Println("Error creating userfile")
	}
	var userActor libs.ActorS
	userActor.PreferredUsername = username
	userActor.Password = password
	for i := 0; i < 32; i++ {
		fmt.Println("")
	}
	userActor.Str = 15
	userActor.Dex = 15
	userActor.Int = 15
	userActor.Wis = 15
	userActor.Con = 15
	userActor.Tec = 1
	userActor.Inventory = map[int]string{
		0: "A glowing disc.",
	}
	userActor.Equipment = map[string]string{
		"body": "A long flowing rainbow gown.",
	}
	userActor.Inbox = hostname + "#inbox/" + username
	userActor.Outbox = hostname + "#outbox/" + username
	userActor.Following = hostname + "#following/" + username
	userActor.Followers = hostname + "#followers/" + username
	userData, err := json.Marshal(&userActor)
	if err != nil {
		fmt.Println("Error marshalling json.")
		return false
	}
	userFile.Write(userData)
	userFile.Sync()
	return true

}
func validate(signature string, file []byte) bool {

	bufferedReader, err := os.Create("tests/outfile")

	if err != nil {
		fmt.Println("Error creating outfile")
	}
	bufferedReader.Write(file)
	bufferedReader.Close()
	decrypter := exec.Command("/bin/sh", "-c", "echo crashandburn | gpg --passphrase-fd 0 -o out.json outfile")
	decrypter.Dir = "tests/"
	decrypter.Run()
	defer os.Remove("tests/out.json")
	UIDFile := "tests/out.json"
	UIDed := updateBork(UIDFile)
	UIDData, err := ioutil.ReadFile(UIDFile)
	if err != nil {
		fmt.Println("\033[2;255;0;0mERROR\033[0m")
	}
	//fmt.Println(UIDed.Id)
	UIDWriter, err := os.Create("tests/" + string(UIDed.Id))

	UIDWriter.Write(UIDData)
	UIDWriter.Close()

	if err != nil {
		fmt.Println("Error running command")
		fmt.Println(err)
		return false
	}
	return true
}

func runHackTrace(user string, target string, initializedActors []libs.ActorS) string {

	userList := strings.Split(user, ":")
	user = userList[0]
	//index := 0
	for i := range initializedActors {
		if initializedActors[i].PreferredUsername == user {
			//	index = i
			break
		}
	}
	//if initializedActors[i].Stat > hitCalc()
	//	return "OK"
	//else
	//  return "NO"
	return "OK"
}
func runBattleTrace(user string, target string, initializedActors []libs.ActorS) string {

	userList := strings.Split(user, ":")
	user = userList[0]
	//index := 0
	for i := range initializedActors {
		if initializedActors[i].PreferredUsername == user {
			//	index = i
			break
		}
	}
	//if initializedActors[i].Stat > hitCalc()
	//	return "OK"
	//else
	//  return "NO"
	return "OK"
}
func upBork(borks []libs.ActorS) []byte {
	borkFile, err := os.Create("tests/borks.json")
	defer borkFile.Close()
	if err != nil {
		fmt.Println("\033[38;2;255;0;0mBork error\033[0m")
	}
	borkJson, err := json.Marshal(borks)
	if err != nil {
		fmt.Println("error marshalling borks")
		fmt.Println(err)
	}
	//fmt.Println(borkJson)
	borkFile.Write(borkJson)
	return borkJson

}
func updateBork(currentActors string) libs.ActorS {
	actorFile, err := os.Open(currentActors)
	defer actorFile.Close()
	if err != nil {
		fmt.Println("Error, cannot find current actor file.")
	}
	actorReader, err := ioutil.ReadAll(actorFile)
	if err != nil {
		fmt.Println("Error reading actorfile.")
	}
	var mockup libs.ActorS
	container := make(map[string]interface{})
	err = json.Unmarshal(actorReader, &mockup)
	err = json.Unmarshal(actorReader, &container)
	if err != nil {
		fmt.Println("Error unmarshalling json.")
		fmt.Println(err)
	}
	k := make([]string, len(container))
	index := 0
	for i, _ := range container {
		k[index] = i

		if k[index] == "@context" {
			con := container["@context"]
			mockup.Context = con
		}
		index++
	}
	//fmt.Println(k)
	//fmt.Println(string(actorReader))
	//fmt.Println(mockup)

	return mockup
}

func initActors(currentActors string) libs.ActorS {
	actorFile, err := os.Open(currentActors)
	defer actorFile.Close()
	if err != nil {
		fmt.Println("Error, cannot find current actor file.")
	}
	actorReader, err := ioutil.ReadAll(actorFile)
	if err != nil {
		fmt.Println("Error reading actorfile.")
	}
	var mockup libs.ActorS
	container := make(map[string]interface{})
	err = json.Unmarshal(actorReader, &mockup)
	err = json.Unmarshal(actorReader, &container)
	if err != nil {
		fmt.Println("Error unmarshalling json.")
		fmt.Println(err)
	}
	k := make([]string, len(container))
	index := 0
	for i, _ := range container {
		k[index] = i

		if k[index] == "@context" {
			con := container["@context"]
			mockup.Context = con
		}
		index++
	}
	//fmt.Println(k)
	//fmt.Println(string(actorReader))
	//fmt.Println(mockup)
	emptyString := mockup
	return emptyString
}
func UIDMaker(typeS string) string {
	hostname := "localhost"
	username := "username"
	//Inspired by 'Una (unascribed)'s bikeshed
	rand.Seed(int64(time.Now().Nanosecond()))
	adjectives := []string{"Accidental", "Allocated", "Asymptotic", "Background", "Binary",
		"Bit", "Blast", "Blocked", "Bronze", "Captured", "Classic",
		"Compact", "Compressed", "Concatenated", "Conventional",
		"Cryptographic", "Decimal", "Decompressed", "Deflated",
		"Defragmented", "Dirty", "Distinguished", "Dozenal", "Elegant",
		"Encrypted", "Ender", "Enhanced", "Escaped", "Euclidean",
		"Expanded", "Expansive", "Explosive", "Extended", "Extreme",
		"Floppy", "Foreground", "Fragmented", "Garbage", "Giga", "Gold",
		"Hard", "Helical", "Hexadecimal", "Higher", "Infinite", "Inflated",
		"Intentional", "Interlaced", "Kilo", "Legacy", "Lower", "Magical",
		"Mapped", "Mega", "Nonlinear", "Noodle", "Null", "Obvious", "Paged",
		"Parity", "Platinum", "Primary", "Progressive", "Prompt",
		"Protected", "Quick", "Real", "Recursive", "Replica", "Resident",
		"Retried", "Root", "Secure", "Silver", "SolidState", "Super",
		"Swap", "Switched", "Synergistic", "Tera", "Terminated", "Ternary",
		"Traditional", "Unlimited", "Unreal", "Upper", "Userspace",
		"Vector", "Virtual", "Web", "WoodGrain", "Written", "Zipped"}
	nouns := []string{"AGP", "Algorithm", "Apparatus", "Array", "Bot", "Bus", "Capacitor",
		"Card", "Chip", "Collection", "Command", "Connection", "Cookie",
		"DLC", "DMA", "Daemon", "Data", "Database", "Density", "Desktop",
		"Device", "Directory", "Disk", "Dongle", "Executable", "Expansion",
		"Folder", "Glue", "Gremlin", "IRQ", "ISA", "Instruction",
		"Interface", "Job", "Key", "List", "MBR", "Map", "Modem", "Monster",
		"Numeral", "PCI", "Paradigm", "Plant", "Port", "Process",
		"Protocol", "Registry", "Repository", "Rights", "Scanline", "Set",
		"Slot", "Smoke", "Sweeper", "TSR", "Table", "Task", "Thread",
		"Tracker", "USB", "Vector", "Window"}
	uniquefier := ""

	uniqe := ""
	for i := 0; i < 2; i++ {
		uniq := rand.Intn(15)
		if uniq >= 10 {
			switch uniq {
			case 10:
				uniqe = "A"
			case 11:
				uniqe = "B"
			case 12:
				uniqe = "C"
			case 13:
				uniqe = "D"
			case 14:
				uniqe = "E"
			case 15:
				uniqe = "F"
			}

			uniquefier += uniqe
		} else {
			uniquefier += fmt.Sprint(uniq)
		}
	}
	ind := rand.Intn(len(adjectives))
	indie := rand.Intn(len(adjectives))
	if indie == ind {
		indie = rand.Intn(len(adjectives))
	}
	thedog := rand.Intn(len(nouns))
	uniqueFied := fmt.Sprint(uniquefier, hostname, username, adjectives[ind], adjectives[indie], nouns[thedog])

	//fmt.Println(uniqueFied)

	if typeS == "post" {
		numPosts, err := ioutil.ReadDir("tests/posts/")
		if err != nil {
			fmt.Println("Error, can't find post directory!")
		}
		UID := fmt.Sprint("posts/", len(numPosts)+1, uniqueFied)
		return UID
	}
	if typeS == "user" {
		numUsers, err := ioutil.ReadDir("tests/users/")
		if err != nil {
			fmt.Println("Error, can't find post directory!")
		}
		UID := fmt.Sprint("users/", len(numUsers)+1, uniqueFied)
		return UID
	}
	return "NOT NUMBERWANG"
}
func main() {

	fmt.Println("\033[48;2;0;255;0m\033[38;2;255;0;0mServer started\033[0m")
	dirBorks, err := ioutil.ReadDir("tests/posts/")
	if err != nil {
		fmt.Println("No borks!")
	}
	fmt.Println("DIRBORKS:", dirBorks)
	//room init
	xvar := 17
	yvar := 17
	room := flour.Dough(xvar, yvar)
	room = flour.Oven(room, "X", xvar, yvar)
	room[122].Label = "player"
	//end room init
	//numBorks := len(dirBorks)
	//numStringBorks := strconv.Itoa(numBorks)
	fmt.Println("\033[38;2;20;20;255mInitializing actor accounts\033[0m")
	currentActors, err := ioutil.ReadDir("tests/users/")
	if err != nil {
		fmt.Println("Error reading the user directory!")
	}
	var initializedActors []libs.ActorS
	for i := range currentActors {

		initializedActors = append(initializedActors, initActors("tests/users/"+currentActors[i].Name()))
	}
	//fmt.Println(initializedActors)
	fmt.Println("\033[48;2;255;20;20mInitialization Complete\033[0m")
	//fmt.Println(numStringBorks)
	//	fmt.Println("\033[38;2;0;255;0mThe most recent post is " + numBorks + "\033[0m")
	for {
		mux := http.NewServeMux()
		mux.HandleFunc("/exampleobject", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			if req.Header.Get("Accept") == "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"" {
				fmt.Println("Accept header is correct.")
				w.Header().Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
				//w.Header().Set("Authentication", signature)
				w.WriteHeader(http.StatusOK)
				fmt.Println("Signing test json.")
				signedContent, err := ioutil.ReadFile("tests/post.json")
				if err != nil {
					fmt.Println("Error reading post.")
				}
				//fmt.Println(string(signedContent))
				fmt.Println("Signing complete.")
				w.Write(signedContent)
			}
		})

		mux.HandleFunc("/getuid", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			if req.Header.Get("Accept") == "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"" {
				fmt.Println("Accept header is correct.")
				w.Header().Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
				UID := UIDMaker("post")
				w.Header().Set("UID", UID)
				w.WriteHeader(http.StatusOK)
				fmt.Println("Signing test json.")
				signedContent, err := ioutil.ReadFile("tests/post.json")
				if err != nil {
					fmt.Println("Error reading file")
				}
				//fmt.Println(string(signedContent))
				fmt.Println("Signing complete.")
				w.Write(signedContent)
			}
		})

		mux.HandleFunc("/exampleMobile", func(w http.ResponseWriter, req *http.Request) {
			//AVATARCOMMANDS

			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			if req.Header.Get("Accept") == "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"" {
				fmt.Println("Accept header is correct.")
				w.Header().Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
				//w.Header().Set("Authentication", signature)
				mobile := req.Header.Get("Mobile-Name")
				w.WriteHeader(http.StatusOK)
				fmt.Println("Signing test json.")
				if err != nil {
					fmt.Println("Error, user directory empty!")
				}
				createMobile(mobile)
				if err != nil {
					fmt.Println("Error reading file")
				}
				w.Write([]byte("bep"))
			}
		})

		mux.HandleFunc("/examplePC", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			if req.Header.Get("Accept") == "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"" {
				fmt.Println("Accept header is correct.")
				w.Header().Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
				//w.Header().Set("Authentication", signature)
				w.WriteHeader(http.StatusOK)
				fmt.Println("Signing test json.")
				names, err := ioutil.ReadDir("tests/users/")
				if err != nil {
					fmt.Println("Error, user directory empty!")
				}
				signedContent, err := ioutil.ReadFile("tests/users/" + names[0].Name())
				if err != nil {
					fmt.Println("Error reading file")
				}
				//fmt.Println(string(signedContent))
				fmt.Println("Signing complete.")
				w.Write(signedContent)
			}
		})

		mux.HandleFunc("/exampleactor", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			if req.Header.Get("Accept") == "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"" {
				fmt.Println("Accept header is correct.")
				w.Header().Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
				//w.Header().Set("Authentication", signature)
				w.WriteHeader(http.StatusOK)
				fmt.Println("Signing test json.")
				signedContent, err := ioutil.ReadFile("tests/post.json")
				if err != nil {
					fmt.Println("Error reading file")
				}
				//fmt.Println(string(signedContent))
				fmt.Println("Signing complete.")
				w.Write(signedContent)
			}
		})
		mux.HandleFunc("/getBattleTrace", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			fmt.Println("WHO FIGHTING?")
			signed := req.Header.Get("Signature")
			target := req.Header.Get("Target")
			//			signature := req.Header.Get("Authorization")
			//signature, err := ioutil.ReadAll(req.Body)
			//fmt.Println(string(signed))
			if err != nil {
				fmt.Println(err)
			}
			//ok := validateLogin(signed, []byte(signature), initializedActors)
			ok := true
			if ok {
				w.Header().Set("Authorization", "ok")
				w.WriteHeader(http.StatusOK)
				//fmt.Println("GOT" + signed)

				results := runBattleTrace(signed, target, initializedActors)
				w.Header().Set("HackTrace", results)
				w.Header().Set("Target", results)
				w.Write([]byte(results))
			} else {
				w.Header().Set("Authorization", "not authorized")
				w.Write([]byte("ennt!"))
			}
		})
		mux.HandleFunc("/getHackTrace", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			fmt.Println("WHAT DOING?")
			signed := req.Header.Get("Signature")
			target := req.Header.Get("Target")
			//			signature := req.Header.Get("Authorization")
			//	signature, err := ioutil.ReadAll(req.Body)
			//fmt.Println(string(signed))
			if err != nil {
				fmt.Println(err)
			}
			//ok := validateLogin(signed, []byte(signature), initializedActors)
			ok := true
			if ok {
				w.Header().Set("Authorization", "ok")
				w.WriteHeader(http.StatusOK)
				fmt.Println("GOT" + signed)
				results := runHackTrace(signed, target, initializedActors)
				w.Write([]byte(results))
			} else {
				w.Header().Set("Authorization", "not authorized")
				w.Write([]byte("ennt!"))
			}
		})
		mux.HandleFunc("/getMap", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			fmt.Println("WHER GOING?")
			//signed := req.Header.Get("Signature")
			//			signature := req.Header.Get("Authorization")
			//	signature, err := ioutil.ReadAll(req.Body)
			//fmt.Println(string(signed))
			//	if err != nil {
			//		fmt.Println(err)
			//	}
			//ok := validateLogin(signed, []byte(signature), initializedActors)
			ok := true
			if ok {
				w.Header().Set("Authorization", "ok")
				dir := req.Header.Get("Direction")

				w.Header().Set("Room Desc", "A lone room in a love hotel:There is little in this room of interest.:A door into the verse stands to the east.")
				w.Header().Set("Room Actions", "Your body is rezzed into being.")
				w.WriteHeader(http.StatusOK)

				switch dir {
				case "east":
					for i := range room {
						if room[i].Label == "player" {
							if room[i+1].Label != "X" {
								room[i+1].Label = "player"
								room[i].Label = ""
								break
							}
							fmt.Printf("You cannot go that way!")
						}
					}
				case "west":
					for i := range room {
						if room[i].Label == "player" {
							if room[i-1].Label != "X" {
								room[i-1].Label = "player"
								room[i].Label = ""
								break
							}
							fmt.Printf("You cannot go that way!")

						}
					}
				case "north":
					for i := range room {
						if room[i].Label == "player" {
							if room[i-xvar].Label != "X" {
								room[i-xvar].Label = "player"
								room[i].Label = ""
								break
							}
							fmt.Printf("You cannot go that way!")

						}
					}
				case "south":
					for i := range room {
						if room[i].Label == "player" {
							if room[i+xvar].Label != "X" {
								room[i+xvar].Label = "player"
								room[i].Label = ""
								break
							}
							fmt.Printf("You cannot go that way!")

						}
					}
				case "northeast":
					for i := range room {
						if room[i].Label == "player" {
							if room[i-xvar+1].Label != "X" {
								room[i-xvar+1].Label = "player"
								room[i].Label = ""
								break
							}
							fmt.Printf("You cannot go that way!")

						}
					}
				case "northwest":
					for i := range room {
						if room[i].Label == "player" {
							if room[i-xvar-1].Label != "X" {
								room[i-xvar-1].Label = "player"
								room[i].Label = ""
								break
							}
							fmt.Printf("You cannot go that way!")

						}
					}
				case "southeast":
					for i := range room {
						if room[i].Label == "player" {
							if room[i+xvar+1].Label != "X" {
								room[i+xvar+1].Label = "player"
								room[i].Label = ""
								break
							}
							fmt.Printf("You cannot go that way!")

						}
					}
				case "southwest":
					for i := range room {
						if room[i].Label == "player" {
							if room[i+xvar-1].Label != "X" {
								room[i+xvar-1].Label = "player"
								room[i].Label = ""
								break
							}
							fmt.Printf("You cannot go that way!")

						}
					}
				}
				space := ""
				for x := range room {
					done := false
					if room[x].Label == "player" {
						space += "@"
						done = true
					}
					if room[x].X > 3 && room[x].Y == 7 && room[x].X < 14 && done == false {
						space += " "
						room[x].Label = " "
						done = true
					}
					if room[x].X > 3 && room[x].Y == 8 && room[x].X < 14 && done == false {
						space += " "
						room[x].Label = " "
						done = true
					}
					if room[x].X > 3 && room[x].Y == 6 && room[x].X < 14 && done == false {
						space += " "
						done = true
						room[x].Label = " "
					}
					if done == false {
						space += "X"
						room[x].Label = "X"
					}
				}
				w.Write([]byte(space))
			} else {
				w.Header().Set("Authorization", "not authorized")
				w.Write([]byte("ennt!"))
			}
		})

		mux.HandleFunc("/examplelogin", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			fmt.Println("I HEAR BORK")
			//signed := req.Header.Get("Signature")
			//			//signature := req.Header.Get("Authorization")
			//	signature, err := ioutil.ReadAll(req.Body)
			//fmt.Println(string(signed))
			//	if err != nil {
			//		fmt.Println(err)
			//	}
			//	ok := validateLogin(signed, []byte(//signature), initializedActors)
			ok := true
			if ok {
				w.Header().Set("Authorization", "ok")
				w.WriteHeader(http.StatusOK)
				w.Write([]byte("bop"))
			} else {
				w.Header().Set("Authorization", "not authorized")
				w.Write([]byte("ennt!"))
			}
		})
		mux.HandleFunc("/examplepost", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			posts, err := ioutil.ReadDir("tests/posts/")
			if err != nil {
				fmt.Println(err)
			}
			numPosts := strconv.Itoa(len(posts))
			w.Header().Set("NumBorks", numPosts)
			w.WriteHeader(http.StatusOK)
			fmt.Println("Signing test json.")
			if req.Header.Get("Accept") == "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"" {
				fmt.Println("Accept header is correct.")
				w.Header().Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
				//w.Header().Set("Authentication", //signature)

				var borks []libs.ActorS
				for _, v := range posts {
					borks = append(borks, updateBork("tests/posts/"+v.Name()))

				}
				upBork(borks)
				signedContent, err := ioutil.ReadFile("tests/borks.json")
				if err != nil {
					fmt.Println("Error reading file")
				}
				//fmt.Println(string(signedContent))
				fmt.Println("Signing complete.")
				w.Write(signedContent)

			}

		})
		mux.HandleFunc("/post", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			//w.Header().Set("NumBorks", numPosts)
			w.WriteHeader(http.StatusOK)
			fmt.Println("Signing test json.")
			if req.Header.Get("Accept") == "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"" {
				fmt.Println("Accept header is correct.")
				w.Header().Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
				//w.Header().Set("Authentication", //signature)

				signedContent, err := ioutil.ReadAll(req.Body)
				if err != nil {
					panic(err)
				} else {
					output := fmt.Sprint("======================", signedContent, "========================")
					fmt.Println(output)
				}
				//TODO
				name := UIDMaker("post")
				file, err := os.Create("tests/"+name)
				defer file.Close()
				numBytes, err := file.WriteString(string(signedContent))
				if err != nil {
					fmt.Println(name)
					fmt.Println("ERROR CREATING FILE")
					panic(err)
				}else {
					fmt.Println("Wrote ", numBytes, " bytes")
					file.Sync()
				}
				var borks []libs.ActorS
				posts, err := ioutil.ReadDir("tests/posts/")
				if err != nil {
					fmt.Println(err)
				}
				for _, v := range posts {
					borks = append(borks, updateBork("tests/posts/"+v.Name()))

				}
				//numPosts := strconv.Itoa(len(posts))
				upBork(borks)
				w.Write(signedContent)

			}

		})

		//CREATE USER

		//END CREATE USER
		//		mux.HandleFunc("/.well-known/webfinger?resource=acct:guarddoggo@example.com", func(w http.ResponseWriter, req *http.Request) {
		// Before any call to WriteHeader or Write, declare
		// the trailers you will set during the HTTP
		// response. These three headers are actually sent in
		// the trailer.
		//			fmt.Println("SOMEONE IS PREPARING TO BORK.")

		mux.HandleFunc("/exampleposter", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			fmt.Println("SOMEONE IS BORKING.")

			//defer resp.Body.Close()

			object, err := ioutil.ReadAll(req.Body)
			req.Body.Close()
			if err != nil {
				fmt.Println("Error reading reponse.")
				fmt.Println(err)
			}
			//beachBall := object
			//fmt.Println(beachBall)
			UID := UIDMaker("post")
			validate(UID, object)

		})
		prefix := os.Getenv("GOPATH")

		mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
			w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
			w.Write([]byte("This is an example server.\n"))
		})
		cfg := &tls.Config{
			MinVersion:               tls.VersionTLS12,
			CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
			PreferServerCipherSuites: true,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
				tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_RSA_WITH_AES_256_CBC_SHA,
			},
		}
		srv := &http.Server{
			Addr:         ":8080",
			Handler:      mux,
			TLSConfig:    cfg,
			TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
		}
		log.Fatal(srv.ListenAndServeTLS(prefix+"/gitlab.com/owlo/server.rsa.crt", prefix+"/gitlab.com/owlo/server.rsa.key"))

	}
}
